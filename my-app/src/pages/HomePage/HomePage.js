import React from 'react';
import Main from '../../components/Main/Main'
import { useSelector} from "react-redux";
const HomePage = (props) => {
const data = useSelector((state)=> state.cards.cards)

    return (
        <>
        {data && <Main data={data} />}

        </>
    )
}
export default HomePage;