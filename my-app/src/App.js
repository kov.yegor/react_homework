import React from "react";
import Modal from "./components/Modal/Modal";
import Header from "./components/Header/Header";
import AppRoutes from "./Routes/Routes";
import {getDataAC} from "./store/actionCreators/cardsAC";
import { useEffect } from "react";
import {useDispatch} from "react-redux";

const App = () => {
  const dispatch = useDispatch()
  useEffect(()=>{
        dispatch(getDataAC())
},[]);

  return (
    <>
      <Header />
      <Modal />
      <AppRoutes
      />
    </>
  );
};
export default App;
