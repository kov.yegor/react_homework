import React from 'react';

import {Routes, Route} from "react-router-dom";
import HomePage from '../pages/HomePage/HomePage';
import CartPage from '../pages/CartPage/CartPage';
import FavPage from '../pages/FavPage/FavPage'
import FormPage from '../pages/FormPage/FormPage'


const AppRoutes = (props) => {

    return (
        <Routes>
            <Route path='/' element={<HomePage />}/>
            <Route path='/cart' element={<CartPage />}/>
            <Route path='/fav' element={<FavPage />}/>
            {/* <Route path='/form' element={<FormPage />}/> */}
        </Routes>
    )
}
export default AppRoutes;