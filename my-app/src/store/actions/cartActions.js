export const ADD_CART_ITEM = "ADD_CART_ITEM";
export const DELETE_CART_ITEM = 'DELETE_CART_ITEM';
export const PLUS_CART_ITEM = " PLUS_CART_ITEM";
export const MINUS_CART_ITEM = " MINUS_CART_ITEM";
export const CLEAR_CART = " CLEAR_CART";
