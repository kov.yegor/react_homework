import {ADD_FAV_ITEM, DELETE_FAV_ITEM} from "../actions/favActions";

export const addFavItemAC = (data)=> {
    return {type: ADD_FAV_ITEM, payload: data}
}
export const deleteFavItemAC = (id) =>{
    return {type: DELETE_FAV_ITEM, payload: id}
}