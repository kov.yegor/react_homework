import {
  GET_DATA,
  REMOVE_DATA,
  SET_IS_LOADING_CARDS,
} from "../actions/cardsActions";
const initialValues = {
  cards: [],
  isLoading: false,
};

const cardsReducer = (state = initialValues, action) => {
  switch (action?.type) {
    case GET_DATA: {
      return { ...state, cards: action.payload };
    }
    case REMOVE_DATA: {
      return { ...state, cards: [] };
    }
    case SET_IS_LOADING_CARDS: {
      return { ...state, isLoading: action.payload };
    }
    default:
      return state;
  }
};

export default cardsReducer;
