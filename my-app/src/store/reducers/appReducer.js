import { combineReducers } from "redux";
import cardsReducer from "./cardsReducer";
import cartReducer from "./cartReducer";
import modalReducer from "./modalReducer";
import favReducer from "./favReducer";

const appReducer = combineReducers({
  cards: cardsReducer,
  cart: cartReducer,
  modal: modalReducer,
  fav: favReducer,
  });

export default appReducer;
