import {
  ADD_CART_ITEM,
  DELETE_CART_ITEM,
  PLUS_CART_ITEM,
  MINUS_CART_ITEM,
  CLEAR_CART,
} from "../actions/cartActions";
import { getItemFromLS, setItemToLS } from "../../utils/localStorage";
getItemFromLS("cart");

const initialValues = {
  cards: getItemFromLS("cart") ? getItemFromLS("cart").cards : [],
};

const cartReducer = (state = initialValues, action) => {
  switch (action.type) {
    case ADD_CART_ITEM: {
      const { name, url, id } = action.payload;

      const index = state.cards.findIndex(
        (item) => item.id === action.payload.id
      );
     
      if (index === -1) {
        const newState = {
          ...state,
          cards: [...state.cards, { name, url, id, count: 1 }],
        };

        setItemToLS("cart", newState);
        return newState;
      }

      const newCartData = [...state.cards];
      newCartData[index].count += 1;

      setItemToLS("cart", { ...state, cards: newCartData });
      return { ...state, cards: newCartData };
    }
    case DELETE_CART_ITEM: {
      const index = state.cards.findIndex((item) => item.id === action.payload);

      if (index === -1) {
        return state;
      }
      const newCartData = [...state.cards];
      newCartData.splice(index, 1);

      setItemToLS("cart", { ...state, cards: newCartData });
      return { ...state, cards: newCartData };
    }
    case PLUS_CART_ITEM: {
      const index = state.cards.findIndex((item) => item.id === action.payload);
      const newCartData = [...state.cards];
      newCartData[index].count += 1;
      setItemToLS("cart", { ...state, cards: newCartData });
      return { ...state, cards: newCartData };
    }
    case MINUS_CART_ITEM: {
      const index = state.cards.findIndex((item) => item.id === action.payload);
      const newCartData = [...state.cards];
      if (newCartData[index].count <= 1) {
        newCartData.splice(index, 1);
        setItemToLS("cart", { ...state, cards: newCartData });
        return { ...state, cards: newCartData };
      }
      newCartData[index].count -= 1;
      setItemToLS("cart", { ...state, cards: newCartData });
      return { ...state, cards: newCartData };
    }
    case CLEAR_CART:{
      setItemToLS("cart", { ...state, cards: [] });
      return { ...state, cards: [] };
    }
    default:
      return state;
  }
};

export default cartReducer;
