import { getItemFromLS, setItemToLS } from "../../utils/localStorage";
import { ADD_FAV_ITEM, DELETE_FAV_ITEM } from "../actions/favActions";
getItemFromLS("fav");

const initialValues = {
  cards: getItemFromLS("fav") ? getItemFromLS("fav").cards : [],
};

const favReducer = (state = initialValues, action) => {
  switch (action.type) {
    case ADD_FAV_ITEM: {
      const { name, url, id } = action.payload;

      const index = state.cards.findIndex(
        (item) => item.id === action.payload.id
      );
 
      if (index === -1) {
        const newState = {
          ...state,
          cards: [...state.cards, { name, url, id }],
        };
        setItemToLS("fav", newState);
        const newFavData = [...state.cards];
        newFavData.splice(index, 1);
        setItemToLS("fav", newState);
        return newState;
      }

      const newFavData = [...state.cards];
      newFavData.splice(index, 1);
      setItemToLS("fav", { ...state, cards: newFavData });
      return { ...state, cards: newFavData };
    }
    case DELETE_FAV_ITEM: {
      const index = state.cards.findIndex((item) => item.id === action.payload);
     
      if (index === -1) {
        return state;
      }
      const newFavData = [...state.cards];
      newFavData.splice(index, 1);

      setItemToLS("fav", { ...state, cards: newFavData });
      return { ...state, cards: newFavData };
    }
    default:
      return state;
  }
};

export default favReducer;
