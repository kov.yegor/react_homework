import React from "react";
import styles from "./Header.module.scss";
import { Link } from "react-router-dom";


class Header extends React.Component {
  render() {
    return (
      <>
        <div className={styles.header}>
          <nav>
            <ul>
              <Link to="/">Home</Link>
              <Link to="/cart">Cart</Link>
              <Link to="/fav">Favorite</Link>
              {/* <Link to="/form">Form</Link> */}
            </ul>
          </nav>
        </div>
      </>
    );
  }
}

export default Header;
