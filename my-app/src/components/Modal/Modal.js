import React from 'react';
import styles from './Modal.module.scss';
import Button from '../Button/Button'
import {useDispatch, useSelector} from "react-redux";
import {setIsOpenModal} from "../../store/actionCreators/modalAC";
import {deleteCartItem} from "../../store/actionCreators/cartAC";

const Modal= () => {
    const isOpen = useSelector(state => state.modal.isOpen)
    const config = useSelector(state => state.modal.config)
const dispatch = useDispatch()
    if (!isOpen) return null;

    const closeModal = ()=>{
        dispatch(setIsOpenModal(false))
    }
    const handleYes = () =>{
        dispatch(deleteCartItem(config.id))
        dispatch(setIsOpenModal(false))
    }
    return (
         <div className={styles.root}>
             <div onClick={closeModal} className={styles.background} />
             <div className={styles.content}>
                 <div className={styles.closeWrapper}>
                     <Button handleClick={closeModal} className={styles.btn} title='X' ></Button>
                 </div>
                 <h2> Want to delete {config && config.name} ?</h2>
                 <div className={styles.buttonContainer}>
                     <Button handleClick={handleYes} title='Continue'></Button>
                     <Button handleClick={closeModal} title='Cancel'></Button>
                 </div>
             </div>
         </div>
    );
};

export default Modal;
