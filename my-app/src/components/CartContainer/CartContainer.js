import React from 'react';
import styles from './CartContainer.module.scss'
import CartItem from '../../components/CartItem/CartItem'
import FormContainer from '../../components/FormContainer/FormContainer'
import { useSelector } from 'react-redux';

const CartContainer = () => {
  const cartData = useSelector((state)=>state.cart.cards)

  return (
    <section className={styles.root}>
     
        <div className={styles.container}>

        {cartData.map((elem)=><CartItem count={elem.count} name={elem.name} id={elem.id} img={elem.url} key={elem.id}  />)}
        </div>
        <FormContainer/>
    </section>
)



}

export default CartContainer;