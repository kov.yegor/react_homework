import React from "react";
import styles from "./Item.module.scss";
import Button from "../Button/Button";
import { ReactComponent as StarIcon } from "../../assets/star-plus.svg";
import {useDispatch} from "react-redux";
import {addCartItemAC} from "../../store/actionCreators/cartAC";
import {addFavItemAC} from "../../store/actionCreators/favAC"

const Item = (props) => {
  const { name, price, id, url } = props;
  const dispatch = useDispatch()

  return (
    <div className={styles.root}>
      <div className={styles.favourites} onClick={() =>{
        dispatch(addFavItemAC({
          name, url, id
          })
          )}
          }>
     <StarIcon />
      </div>
      <p>{name}</p>
      <span>{price}</span>

      <img src={url} alt={name} />
      <Button handleClick={()=>
         dispatch(addCartItemAC({name, url, id}))} 
         title="Add to cart" />
    </div>
  );
};

export default Item;
