import React from "react";
import ItemsContainer from "../ItemsContainer/ItemsContainer";
import styles from "./Main.module.scss";

const Main = (props) => {
  const {data} = props
  return (
    <div className={styles.container}>
      {data &&  <ItemsContainer data={data}
      />}
     
    </div>
  );
};

export default Main;
