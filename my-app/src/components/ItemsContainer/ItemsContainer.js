import React from "react";
import styles from "./ItemsContainer.module.scss";
import Item from "../Item/Item";
import {useSelector} from "react-redux";
const ItemsContainer = (props) => {
  const isLoading = useSelector(state => state.cards.isLoading)
  const { data } = props;

  return (
    <section className={styles.root}>
      <div className={styles.container}>
        {!isLoading &&
          data.map((elem) => (
          <Item  id={elem.id} url={elem.url} price={elem.price} key={elem.id} name={elem.name}/>
          ))}
      </div>
    </section>
  );
};

export default ItemsContainer;
