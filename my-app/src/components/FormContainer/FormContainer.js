import React from 'react';
import styles from './FormContainer.module.scss'
import { useDispatch } from "react-redux";
import {getItemFromLS} from "../../utils/localStorage"
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as yup from "yup";
import Button from "../Button/Button";
import { clearCartAC } from '../../store/actionCreators/cartAC';
import { useNavigate } from "react-router";


const FormContainer = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    
    const initialValues = {
        firstName: "",
        lastName: "",
        age: "",
        adress: "",
        phone: "",
      }; 
      const schema = yup.object().shape({
        firstName:yup.string().required('The field is required') , 
        lastName: yup.string().required('The field is required') ,
        age: yup.number('only number').max(85).min(18).positive().required('The field is required'),
        adress:yup.string().required('The field is required') ,
        phone: yup.number().positive().required('The field is required'),
    });
    const onSubmit = async (values, {resetForm}) => {
        try {
          const cartData = getItemFromLS('cart')
          console.log(cartData);
          console.log(values);
         dispatch(clearCartAC())
          resetForm();
          navigate("/");
        } catch (error) {
          console.log(error);
        }
      };
    
    

    return (
        <>
        <Formik
      initialValues={initialValues}
      onSubmit={onSubmit}
      validationSchema={schema}
    >
      <Form className={styles.formContainer}>
        <div className={styles.inputContainer}>
          <label>First Name</label>
          <Field name="firstName" type="name" className={styles.input} placeholder='First name'/>
          <ErrorMessage
            name="firstName"
            render={msg => <span className={styles.error}>{msg}</span>}
          />
        </div>
        <div className={styles.inputCont}>
          <label>Last Name</label>
          <Field name="lastName" type="name" className={styles.input}  placeholder='Last name'/>
          <ErrorMessage
            name="lastName"
            render={msg => <span className={styles.error}>{msg}</span>}
          />
        </div>
        <div className={styles.inputCont}>
          <label>Age</label>
          <Field name="age" type="age" className={styles.input}  placeholder='Age'/>
          <ErrorMessage name="age"  render={msg => <span className={styles.error}>{msg}</span>} />
        </div>
        <div className={styles.inputCont}>
          <label>Adress</label>
          <Field name="adress" type="adress" className={styles.input}  placeholder='Adress'/>
          <ErrorMessage
            name="adress"
            render={    msg => <span className={styles.error}>{msg}</span>}
          />
        </div>
        <div className={styles.inputCont}>
          <label>Phone </label>
          <Field
            name="phone"
            type="tel"
            className={styles.input}
            placeholder='Your phone'
          />
          <ErrorMessage
            name="phone"
            render={msg => <span className={styles.error}>{msg}</span>}
          />
        </div>
        <div>
          <Button type="submit" title="Checkout"/>
        
        </div>
      </Form>
    </Formik>

        </>
    )
}

export default FormContainer
