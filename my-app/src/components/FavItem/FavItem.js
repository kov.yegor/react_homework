import React from 'react';
import styles from './FavItem.module.scss';
import Button from '../Button/Button'
import { useDispatch } from 'react-redux';
import {setConfigModal, setIsOpenModal} from "../../store/actionCreators/modalAC";
import { deleteFavItemAC} from "../../store/actionCreators/favAC";

const FavItem = (props) => {
    const {price, name, id, img} = props
    const dispatch = useDispatch()
    const handleDelete = () =>{
        dispatch(deleteFavItemAC(id))
        dispatch(setIsOpenModal(true))
    dispatch(setConfigModal({name, id}))
    }
    return (
        <>

        <div className={styles.cartItem}>
            <div className={styles.contentContainer}>
                <div className={styles.imgWrapper}>
                    <img className={styles.itemAvatar} src={img} alt={name}/>
                </div>
                <span className={styles.title}>{name}</span>
                <span className={styles.title}>{price}</span>
                <Button title='Delete' handleClick={handleDelete}/>
            </div>
        </div>
            </>
    )
}

export default FavItem;