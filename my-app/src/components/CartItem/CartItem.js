import React from 'react';
import styles from './CartItem.module.scss';
import Button from '../Button/Button'
import { useDispatch } from 'react-redux';
import {setConfigModal, setIsOpenModal} from "../../store/actionCreators/modalAC";

const CartItem = (props) => {
    const {count, name, id, img} = props;
    const dispatch = useDispatch()
const handleDelete = () =>{
    dispatch(setIsOpenModal(true))
    dispatch(setConfigModal({name, id}))
}
    return (
        <>

        <div className={styles.cartItem}>
            <div className={styles.contentContainer}>
                <div className={styles.imgWrapper}>
                    <img className={styles.itemAvatar} src={img} alt={name}/>
                </div>
                <span className={styles.title}>{name}</span>
            </div>


            <span className={styles.quantity}>{count}</span>

            <div className={styles.btnContainer}>
                <Button  handleClick={handleDelete} title='Delete'></Button>
            </div>

        </div>
            </>
    )
    }

export default CartItem;