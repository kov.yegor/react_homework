import React from 'react'
import styles from './Button.module.scss'

const Button =(props)=>{
const { title, handleClick} = props

    return (
        <>
         <button className={styles.button}  onClick={handleClick}>{title}</button>
        </>
    )
}
 export default Button;
